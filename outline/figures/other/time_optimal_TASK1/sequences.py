# SEQUENCES.PY
#
# A library of commonly used compensating pulse sequences.
# 
# Copyright (C) 2011, 2012 True Merrill
# Georgia Institute of Technology
# School of Chemistry and Biochemistry
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from qudy import *
from qudy.quantop import *

__all__ = ["SK1", "SK2", "SK2_rhombus", "B2", "B2_symmetric", "B2j", \
           "B2j_symmetric", "N2", "N2_symmetric", "N2j", \
           "N2j_symmetric", "P2", "P2_symmetric", "P2j", "P2j_symmetric", \
           "ASK1", "TASK1", "AN2", "TAN2", "AN2j", "TAN2j"]


def SK1( upsilon, phi, err ):
    """
    Solovay-Kitaev first-order compensation sequence.  The sequence may
    be used to perform a compensated R(upsilon,phi) operation, i.e. a
    one-qubit rotation.
    
    Keyword arguments;
    * ``upsilon``  Angle of the desired rotation
    * ``phi`` Phase of the desired rotation
    * ``err`` Error model for the system
    """
    phi1 = arccos( - upsilon/(4*pi) )
    return M(2*pi, phi-phi1, err) * \
           M(2*pi, phi+phi1, err) * \
           M(upsilon,phi, err)


def ASK1(alpha, upsilon, err, mirrored = False):
    """
    Returns a propagator object representing an incomplete compensation sequence
    based on SK1.  It has not been moved back into the plane.
    
    alpha:      angle to replace 2*pi in the SK1 sequence
    upsilon:      angle to replace the desired angle in SK1
    err:        error object
    """
    # phi is the angle needed for the three rotations to form a closed triangle
    # in the Lie algebra
    phi = arccos( -upsilon/(2*alpha) )
    if mirrored:
        return M(alpha, phi, err) * M(alpha, -phi, err) * M(upsilon, 0, err)
    else:
        return M(alpha, -phi, err) * M(alpha, phi, err) * M(upsilon, 0, err)


def TASK1(alpha, upsilon, err, mirrored = False):
    """
    Returns a propagator object representing a compensation sequence based on
    SK1.
    
    alpha:      angle to replace 2*pi in the SK1 sequence
    upsilon:      angle to replace the desired angel in SK1
    err:        error object
    """
    return planarize(ASK1(alpha, upsilon, err, mirrored))


def SK2( upsilon, phi, err ):
    """
    Solovay-Kitaev second-order compensation sequence.  This sequence
    uses Aram's usual construction, and was derived analytically using
    the technique described in ``Progress in compensating pulse
    sequences for quantum computation``.
    
    Keyword arguments;
    * ``upsilon``  Angle of the desired rotation
    * ``phi`` Phase of the desired rotation
    * ``err`` Error model for the system
    """
    phi_upsilon = arccos( - upsilon/(4*pi) )

    # Calculate magnitude of second order error term.
    Omega2 = 1j * (2*pi**2) * sin(2* phi_upsilon)

    def Sx( alpha, err ):
        c = ceil( abs(alpha) / (4*pi))
        phi_alpha = arccos( alpha / (4*pi*c) )
        return M(2*pi*c, -phi_alpha, err) * \
               M(2*pi*c, phi_alpha, err)

    def Sy( alpha, err ):
        c = ceil( abs(alpha) / (4*pi))
        phi_alpha = arccos( alpha / (4*pi*c) )
        return M(2*pi*c, pi/2.0 - phi_alpha, err) * \
               M(2*pi*c, pi/2.0 + phi_alpha, err)

    # Create corrector seqeuence
    a_ = 2*pi*cos(phi_upsilon)
    b_ = -2*pi*sin(phi_upsilon)
    
    B_2 = Sx(-a_, err) * \
          Sy(-b_, err) * \
          Sx( a_, err) * \
          Sy( b_, err)

    # Multiply and cancel second order term
    return B_2 * SK1( upsilon, phi_upsilon, err )


def SK2_rhombus( upsilon, phi, err ):
    """
    Solovay-Kitaev second-order compensation sequence.  This sequence
    uses the rhombus construction, and was derived analytically using
    the technique described in ``Progress in compensating pulse
    sequences for quantum computation``.
    
    Keyword arguments;
    * ``upsilon``  Angle of the desired rotation
    * ``phi`` Phase of the desired rotation
    * ``err`` Error model for the system
    """

    phi_upsilon = arccos( - upsilon / (4*pi) )
    gamma = - 2*pi**2 * sin( 2*phi_upsilon )
    c = ceil( abs(gamma) / (4*pi) )
    phi_gamma = arcsin( gamma / (4 * pi**2 * c**2) )

    B_2 = M(2*pi, phi + phi_gamma + pi, err) * \
          M(2*pi, phi, err ) * \
          M(2*pi, phi + phi_gamma, err ) * \
          M(2*pi, phi + pi, err )

    return B_2 * SK1(upsilon,phi,err)


def B2( upsilon, phi, err ):
    """
    Wimperis second-order broadband compensation sequence.  The
    sequence may be used to perform a compensated R(upsilon,phi)
    operation, i.e. a one-qubit rotation.
    
    Keyword arguments;
    * ``upsilon``  Angle of the desired rotation
    * ``phi`` Phase of the desired rotation
    * ``err`` Error model for the system    
    """
    phi1 = arccos( - upsilon/(4*pi) )
    return M(pi, phi+phi1, err) * \
           M(2*pi, phi+3*phi1, err) * \
           M(pi, phi+phi1, err) * \
           M(upsilon, phi, err)


def B2_symmetric( upsilon, phi, err ):
    """
    Time-symmetric Wimperis second-order broadband compensation
    sequence.  Enforcing time-reversal symmetry frequently improves
    sequence performance.  The sequence may be used to perform a
    compensated R(upsilon,phi) operation, i.e. a one-qubit rotation.
    
    Keyword arguments;
    * ``upsilon``  Angle of the desired rotation
    * ``phi`` Phase of the desired rotation
    * ``err`` Error model for the system 
    """
    phi1 = arccos( - upsilon/(4*pi) )
    return M(upsilon/2.0, phi, err) * \
           M(pi, phi+phi1, err) * \
           M(2*pi, phi+3*phi1, err) * \
           M(pi, phi+phi1, err) * \
           M(upsilon/2.0, phi, err)


def B2j( upsilon, phi, err, order ):
    """
    Wimperis 2jth-order broadband compensation sequence.  The
    sequence may be used to perform a compensated R(upsilon,phi)
    operation, i.e. a one-qubit rotation.
    
    Keyword arguments;
    * ``upsilon``  Angle of the desired rotation
    * ``phi`` Phase of the desired rotation
    * ``err`` Error model for the system
    * ``order`` Desired order of the sequence.  If order is odd, we
      use j = ceil( order/2 )
    """
    def S(phi1, phi2, m, n):
        # Constructor notation, used in PRA 70,052318 (2004)
        if n == 1:
            return M(m/2.0 *pi,phi + phi1, err) * \
                   M(m*pi,phi + -phi1 + 4*phi1*( (m/2.0)%2 ), err) * \
                   M(m/2.0 *pi,phi + phi1, err)
        elif n > 1:
            return S(phi1,phi2,m,n-1) ** (4**(n-1)) * \
                   S(phi1,phi2,-2*m,n-1) * \
                   S(phi1,phi2,m,n-1) ** (4**(n-1))
        
    def fj(j):
        if j == 1:
            return 1
        else:
            return (2**(2*j-1) - 2) * fj(j-1)
    
    # Round up to the nearest j
    j = ceil( order / 2.0 )
    
    phib = arccos( - upsilon / (4*pi*fj( j ) ))
    return S(phib, 3*phib, 2, j) * M(upsilon,phi,err)


def B2j_symmetric( upsilon, phi, err, order ):
    """
    Time-symmetric Wimperis 2jth-order broadband compensation
    sequence.  Enforcing time-reversal symmetry frequently improves
    sequence performance.  The sequence may be used to perform a
    compensated R(upsilon,phi) operation, i.e. a one-qubit rotation.
    
    Keyword arguments;
    * ``upsilon``  Angle of the desired rotation
    * ``phi`` Phase of the desired rotation
    * ``err`` Error model for the system
    * ``order`` Desired order of the sequence.  If order is odd, we
      use j = ceil( order/2 )
    """
    def S(phi1, phi2, m, n):
        # Constructor notation, used in PRA 70,052318 (2004)
        if n == 1:
            return M(m/2.0 *pi,phi + phi1, err) * \
                   M(m*pi,phi + -phi1 + 4*phi1*( (m/2.0)%2 ), err) * \
                   M(m/2.0 *pi,phi + phi1, err)
        elif n > 1:
            return S(phi1,phi2,m,n-1) ** (4**(n-1)) * \
                   S(phi1,phi2,-2*m,n-1) * \
                   S(phi1,phi2,m,n-1) ** (4**(n-1))
        
    def fj(j):
        if j == 1:
            return 1
        else:
            return (2**(2*j-1) - 2) * fj(j-1)

    # Round up to the nearest j
    j = ceil( order / 2.0 )
    
    phib = arccos( - upsilon / (4*pi*fj( j ) ))
    return M(upsilon/2.0,phi,err) * \
           S(phib, 3*phib, 2, j) * \
           M(upsilon/2,phi,err)


def N2( upsilon, phi, err ):
    """
    Wimperis second-order narrowband compensation sequence.  The
    sequence may be used to perform a compensated R(upsilon,phi)
    operation, i.e. a one-qubit rotation.
    
    Keyword arguments;
    * ``upsilon``  Angle of the desired rotation
    * ``phi`` Phase of the desired rotation
    * ``err`` Error model for the system 
    """
    phi1 = arccos( - upsilon/(4*pi) )
    return M(pi, phi+phi1, err) * \
           M(2*pi, phi-phi1, err) * \
           M(pi, phi+phi1, err) * \
           M(upsilon, phi, err)


def AN2(alpha, upsilon, err):
    """
    Returns a propagator object representing an incomplete compensation sequence
    based on N2 (NB1).  It has not been moved back into the plane.
    
    alpha:      angle to replace pi in the N2 sequence
    upsilon:      angle to replace the desired angle in N2
    err:        error object
    """
    phi = arccos( - upsilon/(4*alpha) )
    return M(alpha, phi, err) * \
           M(2*alpha, -1*phi, err) * \
           M(alpha, phi, err) * \
           M(upsilon, 0, err)


def TAN2(alpha, upsilon, err):
    """
    Returns a propagator object representing a compensation sequence based on
    N2 (NB1).
    
    alpha:      angle to replace pi in the N2 sequence
    upsilon:      angle to replace the desired angle in N2
    err:        error object
    """
    return planarize(AN2(alpha, upsilon, err))


def N2_symmetric( upsilon, phi, err ):
    """
    Time-symmetric Wimperis second-order narrowband compensation
    sequence.  Enforcing time-reversal symmetry frequently improves
    sequence performance.  The sequence may be used to perform a
    compensated R(upsilon,phi) operation, i.e. a one-qubit rotation.
    
    Keyword arguments;
    * ``upsilon``  Angle of the desired rotation
    * ``phi`` Phase of the desired rotation
    * ``err`` Error model for the system 
    """
    phi1 = arccos( - upsilon/(4*pi) )
    return M(upsilon/2.0, phi, err) * \
           M(pi, phi+phi1, err) * \
           M(2*pi, phi-phi1, err) * \
           M(pi, phi+phi1, err) * \
           M(upsilon/2.0, phi, err)


def N2j( upsilon, phi, err, order ):
    """
    Wimperis 2jth-order narrowband compensation sequence.  The
    sequence may be used to perform a compensated R(upsilon,phi)
    operation, i.e. a one-qubit rotation.
    
    Keyword arguments;
    * ``upsilon``  Angle of the desired rotation
    * ``phi`` Phase of the desired rotation
    * ``err`` Error model for the system
    * ``order`` Desired order of the sequence.  If order is odd, we
      use j = ceil( order/2 )
    """
    def S(phi1, phi2, m, n):
        # Constructor notation, used in PRA 70,052318 (2004)
        if n == 1:
            return M(m*pi, phi+phi1, err) * \
                   M(2*m*pi, phi+phi2, err) * \
                   M(m*pi, phi+phi1, err)
        elif n > 1:
            return S(phi1,phi2,m,n-1) ** int(4**(n-1)) * \
                   S(phi1,phi2,-2*m,n-1) * \
                   S(phi1,phi2,m,n-1) ** int(4**(n-1))
        
    def fj(j):
        if j == 1:
            return 1
        else:
            return (2**(2*j-1) - 2) * fj(j-1)

    # Round up to the nearest j
    j = ceil( order / 2.0 )

    phij = arccos( - upsilon / (4*pi*fj( j ) ))
    return S(phij, -phij, 1, j) * M(upsilon, phi, err)


def AN2j(alpha, upsilon, err, order):
    """
    Returns a propagator object representing an incomplete compensation sequence
    based on N2-J.  It has not been moved back into the plane.
    
    alpha:      angle to replace pi in the N2 sequence
    upsilon:      angle to replace the desired angle in N2
    err:        error object
    order:      desired order of the sequence.  If order is odd, we use
                j = ceil(order/2)
    """
    # right now alpha would vary between 0 and pi
    def S(phi1, phi2, m, n):
        # Constructor notation, used in PRA 70,052318 (2004)
        if n == 1:
            return M(m*alpha, phi1, err) * \
                   M(2*m*alpha, phi2, err) * \
                   M(m*alpha, phi1, err)
        elif n > 1:
            return S(phi1,phi2,m,n-1) ** (int(4**(n-1))) * \
                   S(phi1,phi2,-2*m,n-1) * \
                   S(phi1,phi2,m,n-1) ** (int(4**(n-1)))
        
    def fj(j):
        if j == 1:
            return 1
        else:
            return (2**(2*j-1) - 2) * fj(j-1)

    # Round up to the nearest j
    j = ceil( order / 2.0 )
    
    phij = arccos( - upsilon / (4*alpha*fj( j )) )
    return S(phij, -phij, 1, j) * M(upsilon, 0, err)


def TAN2j(alpha, upsilon, err, order):
    return planarize(AN2j(alpha, upsilon, err, order))


def N2j_symmetric( upsilon, phi, err, order ):
    """
    Time-symmetric Wimperis 2jth-order narrowband compensation
    sequence.  Enforcing time-reversal symmetry frequently improves
    sequence performance.  The sequence may be used to perform a
    compensated R(upsilon,phi) operation, i.e. a one-qubit rotation.
    
    Keyword arguments;
    * ``upsilon``  Angle of the desired rotation
    * ``phi`` Phase of the desired rotation
    * ``err`` Error model for the system
    * ``order`` Desired order of the sequence.  If order is odd, we
      use j = ceil( order/2 )
    """
    def S(phi1, phi2, m, n):
        # Constructor notation, used in PRA 70,052318 (2004)
        if n == 1:
            return M(m*pi,phi + phi1, err) * \
                   M(2*m*pi,phi + phi2, err) * \
                   M(m*pi,phi + phi1, err)
        elif n > 1:
            return S(phi1,phi2,m,n-1) ** (4**(n-1)) * \
                   S(phi1,phi2,-2*m,n-1) * \
                   S(phi1,phi2,m,n-1) ** (4**(n-1))
        
    def fj(j):
        if j == 1:
            return 1
        else:
            return (2**(2*j-1) - 2) * fj(j-1)

    # Round up to the nearest j
    j = ceil( order / 2.0 )
        
    phij = arccos( - upsilon / (4*pi*fj( j ) ))
    return M(upsilon/2.0,phi,err) * \
           S(phij, -phij, 1, j) * \
           M(upsilon/2.0,phi,err)


def P2( upsilon, phi , err ):
    """
    Wimperis second-order passband compensation sequence.  The
    sequence may be used to perform a compensated R(upsilon,phi)
    operation, i.e. a one-qubit rotation.
    
    Keyword arguments;
    * ``upsilon``  Angle of the desired rotation
    * ``phi`` Phase of the desired rotation
    * ``err`` Error model for the system 
    """
    phi1 = arccos( - upsilon/(8*pi) )
    return M(2*pi, phi+phi1, err) * \
           M(4*pi, phi-phi1, err) * \
           M(2*pi, phi+phi1, err) * \
           M(upsilon, phi, err)


def P2_symmetric( upsilon, phi, err ):
    """
    Time-symmetric Wimperis second-order passband compensation
    sequence.  Enforcing time-reversal symmetry frequently improves
    sequence performance.  The sequence may be used to perform a
    compensated R(upsilon,phi) operation, i.e. a one-qubit rotation.
    
    Keyword arguments;
    * ``upsilon``  Angle of the desired rotation
    * ``phi`` Phase of the desired rotation
    * ``err`` Error model for the system 
    """
    phi1 = arccos( - upsilon/(8*pi) )
    return M(upsilon/2.0, phi, err) * \
           M(2*pi, phi+phi1, err) * \
           M(4*pi, phi-phi1, err) * \
           M(2*pi, phi+phi1, err) * \
           M(upsilon/2.0, phi, err)


def P2j( upsilon, phi, err, order ):
    """
    Wimperis 2jth-order passband compensation sequence.  The
    sequence may be used to perform a compensated R(upsilon,phi)
    operation, i.e. a one-qubit rotation.
    
    Keyword arguments;
    * ``upsilon``  Angle of the desired rotation
    * ``phi`` Phase of the desired rotation
    * ``err`` Error model for the system
    * ``order`` Desired order of the sequence.  If order is odd, we
      use j = ceil( order/2 )
    """
    def S(phi1, phi2, m, n):
        # Constructor notation, used in PRA 70,052318 (2004)
        if n == 1:
            return M(m*pi, phi + phi1, err) * \
                   M(2*m*pi, phi + phi2, err) * \
                   M(m*pi, phi + phi1, err)
        elif n > 1:
            return S(phi1,phi2,m,n-1) ** (4**(n-1)) * \
                   S(phi1,phi2,-2*m,n-1) * \
                   S(phi1,phi2,m,n-1) ** (4**(n-1))
        
    def fj(j):
        if j == 1:
            return 1
        else:
            return (2**(2*j-1) - 2) * fj(j-1)

    # Round up to the nearest j
    j = ceil( order / 2.0 )
        
    phij = arccos( - upsilon / (8*pi*fj( j ) ))
    return S(phij, -phij, 2, j) * M(upsilon,phi,err)


def P2j_symmetric( upsilon, phi, err, order ):
    """
    Time-symmetric Wimperis 2jth-order passband compensation
    sequence.  Enforcing time-reversal symmetry frequently improves
    sequence performance.  The sequence may be used to perform a
    compensated R(upsilon,phi) operation, i.e. a one-qubit rotation.
    
    Keyword arguments;
    * ``upsilon``  Angle of the desired rotation
    * ``phi`` Phase of the desired rotation
    * ``err`` Error model for the system
    * ``order`` Desired order of the sequence.  If order is odd, we
      use j = ceil( order/2 )
    """
    def S(phi1, phi2, m, n):
        # Constructor notation, used in PRA 70,052318 (2004)
        if n == 1:
            return M(m*pi,phi + phi1, err) * \
                   M(2*m*pi,phi + phi2, err) * \
                   M(m*pi,phi + phi1, err)
        elif n > 1:
            return S(phi1,phi2,m,n-1) ** (4**(n-1)) * \
                   S(phi1,phi2,-2*m,n-1) * \
                   S(phi1,phi2,m,n-1) ** (4**(n-1))
        
    def fj(j):
        if j == 1:
            return 1
        else:
            return (2**(2*j-1) - 2) * fj(j-1)

    # Round up to the nearest j
    j = ceil( order / 2.0 )
        
    phij = arccos( - upsilon / (8*pi*fj( j ) ))
    return M(upsilon/2.0, phi, err) * \
           S(phij, -phij, 2, j) * \
           M(upsilon/2.0,phi,err)

############################ Helper Functions ##################################

def planarize(prop, return_angles = False):
    """
    Moves rotation into the XY plane, preserving the magnitude of the rotation.
    
    prop:   An imperfect propagator object
    """
    # The error is unknown physically, so we rely on the rotations we try to
    # perform
    perfect = propagator(prop.ideal_control)
    # 3D Vector representation of the propagator
    rot_vec = decomp(perfect.solve())
    # Find angle from the X axis
    phi = find_angle(rot_vec)
    # Calculate the angle to the axis about which we will apply our new rotation
    if rot_vec[2] > 0:
        beta = phi + pi/2
    else:
        beta = phi - pi/2
    # Calculate the magnitude of the rotation, note that if Z is negative, gamma
    # will be negative, and will therefore still move towards plane
    if rot_vec[2] < 1e-5:
        gamma = 0
    elif norm(rot_vec[0:2]) == 0:
        gamma = pi/2
    else:
        gamma = arctan(abs(rot_vec[2])/norm(rot_vec[0:2]))

    if return_angles:
        return (gamma, beta)
    else:
        if hasattr(prop, "error"):
            # Note: R(gamma, beta+pi) = R(-gamma, beta)
            return M(gamma, beta, prop.error) * prop * \
                    M(gamma, beta+pi, prop.error)
        else:
            # Note: R(gamma, beta+pi) = R(-gamma, beta)
            return R(gamma, beta) * prop * R(gamma, beta+pi)


def find_angle(rot_vec):
    """
    Silly little function to expand arctangent to give values from
    -pi/2 to 3pi/4.  Will return angle that specifies the location of rot_vec
    completely.
    
    rot_vec:    A list with at least two elements
    """
    if rot_vec[0] == 0:
        if rot_vec[1] < 0:
            phi = -pi/2
        else:
            phi = pi/2
    else:
        phi = arctan( rot_vec[1]/rot_vec[0] )
    if rot_vec[0] < 0:
        phi += pi
    return phi