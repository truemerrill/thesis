import numpy
from qudy import *
import matplotlib.pyplot as plt
from sequences import *

# Set a constant error
epsilon = 0.001
err = error('addressing', epsilon)

# Sample angles
theta_min_time1 = numpy.genfromtxt("TASK1_optimal_time_theta_large.csv")
upsilon_min_time = numpy.genfromtxt("TASK1_optimal_time_upsilon_large.csv")
alpha_min_time = numpy.genfromtxt("TASK1_optimal_time_alpha_large.csv")

# Measure operation times
time_min_time = []
theta_min_time = []

for index in range( len(theta_min_time) ):

    ups = upsilon_min_time[index]
    alph = alpha_min_time[index]

    min_time = TASK1( alph, ups, err )
    t = min_time.timemax() - min_time.timemin()
    time_min_time.append(t)
    th = measure_theta( min_time )
    theta_min_time.append(th)
    print "%.3f\t%.3f" %(th, theta_min_time1[index])


# Sample angles
upsilon_min_error = numpy.arange(0.001, 2 * numpy.pi, 0.001)
theta_min_error = []
time_min_error = []
theta_sk1 = []
time_sk1 = []

def measure_theta( seq ):
    perfect = propagator( seq.ideal_control )
    vec = decomp( perfect.solve() )
    th = numpy.sqrt( vec[0]**2 + vec[1]**2 + vec[2]**2 )
    return th

for index in range( len(upsilon_min_error) ):

    ups = upsilon_min_error[index]
    min_error = TASK1( ups, ups, err )
    t = min_error.timemax() - min_error.timemin()

    # measure rotation angle
    time_min_error.append(t)
    theta_min_error.append( measure_theta(min_error) )

plt.plot( theta_min_time, time_min_time )
plt.show()
