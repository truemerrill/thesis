
% ************************************
% Section 4.1.1
% ************************************


\chapter{Arbitrarily accurate sequences}
\label{SK-method}
In this section, we discuss the Solovay-Kitaev method for constructing
arbitrarily accurate composite pulse sequences, which may be used to
systematically improve the performance of an initial seed sequence.
The Lie algebraic picture is particularly helpful in the description
of the algorithm.  The method is quite general, and can be used on
sequences other than SK1.

Suppose that we have an $n$th-order compensating pulse
sequence, here denoted as $W_n$.  The problem we consider is the
identification of a unitary operator $A_{n+1}$ such that $W_{n+1} =
A_{n+1} W_{n} = U_T + O(\epsilon^{n+2})$, where $W_{n+1}$ is an
$(n+1)$th-order sequence.  Assume for now that such an
operator exists, and consider that in the presence of systematic
errors, the application of the correction gate $A_{n+1}$ is imperfect.
However, if it is possible to implement a compensating pulse sequence
$B_{n+1}$, which is an $O(\epsilon^{n+2})$ approximation of $ A_{n+1}$,
then it is still possible to construct an $(n+1)$th-order
sequence, .
\begin{eqnarray}
W_{n+1} = B_{n+1} W_{n} = U_T + O(\epsilon^{n+2}).
\end{eqnarray}
We may then continue constructing pulse sequences of increasing
accuracy in this fashion if there exists a family of operators
$\{A_{n+1}, A_{n+2}, A_{n+3}, \cdots A_{m}\}$ and a corresponding family of
pulse sequences $\{B_{n+1}, B_{n+2}, B_{n+3}, \cdots B_{m}\}$ which
implement the operators to the required accuracy.  This immediately
suggests an inductive construction for the sequence $W_m$,
\begin{eqnarray}
W_{m} = B_{m} \, B_{m-1} \cdots B_{n+3} \, B_{n+2} \, B_{n+1} \, W_n = U_T + O(\epsilon^{m+1}).
\end{eqnarray}
This is the basis of the Solovay-Kitaev method \cite{Brown2004}.  To
apply the method, we must first have a means of calculating the
correction $A_{n+1}$, and second we must find a compensating sequence
$B_{n+1}$ robust to the systematic error model considered.

We turn our attention to the calculation of the correction terms
$A_{n+1}$.  It is convenient to decompose the pulse sequence
propagator using an interaction frame as $W_n = U_T U^I(\epsilon
\vec{\delta u}(t))$, where $U_T = U(\vec{u}(t))$ is the target gate.  In the spirit of
Eqn. \ref{eq:magnus-cancellation}, a Magnus expansion for $U^I(\epsilon
\vec{\delta u}(t))$ may be used.  Observe that $W_n = U_T \exp( \epsilon^{n+1}
\Omega_{n+1} ) + O(\epsilon^{n+2})$.  Then letting $A_{n+1} = U_T
\exp( - \epsilon^{n+1} \Omega_{n+1} ) U^\dagger_T$ it may be verified
using the BCH formula that $A_{n+1} W_n = U_T + O(\epsilon^{n+2})$.
This result may also be interpreted in terms of vector displacements
on the Lie algebra.  In the interaction frame, $\epsilon^{n+1}
\Omega_{n+1}$ may be interpreted as a vector in $\algb{su}(2)$.  Similarly,
the infinitesimal rotation $A_{n+1}$ corresponds to the vector
$-\epsilon^{n+1} \Omega_{n+1}$, equal in magnitude and opposite in
orientation.  The first-order term of the BCH series corresponds to
vector addition on the Lie algebra, and the $O(\epsilon^{n+1})$ terms
cancel.

What remains is to develop a compensating pulse sequence that
implements $A_{n+1}$ to the required accuracy under a given error
model.  Let us define $P_{j z}(\alpha) = \exp(-\i \alpha \epsilon^j
H_{z}) + O(\epsilon^{j+1})$ and also the rotated analogues $P_{j
  x}(\alpha) = \exp(-\i \alpha \epsilon^j H_{x}) + O(\epsilon^{j+1})$
and $P_{j y}(\alpha) = \exp(-\i \alpha \epsilon^j H_{y}) +
O(\epsilon^{j+1})$. Frequently if one such $P_j$ may be produced,
then often the remaining two may be produced by similarity
transformation of the propagators or by using an Euler decomposition.
At this point we use Eqn. \ref{SK}, used in the proof for the
Solovay-Kitaev theorem, to construct relation
\begin{eqnarray}
%P_{\lfloor j/2 \rfloor x}(-\alpha) P_{\lceil j/2 \rceil y}(-\beta) P_{\lfloor j/2 %\rfloor x}(\alpha) P_{\lceil j/2 \rceil y}(\beta) &=& P_{jz}(\alpha \beta).
P_{kx}(-\alpha) P_{\ell y}(-\beta) P_{kx}(\alpha) P_{\ell y}(\beta) &=& P_{jz}(\alpha \beta), \qquad k + \ell = j.
\label{eq:recursive-Pj}
\end{eqnarray}
Continuing in this manner, each of the $P_j$'s may be recursively
decomposed into a product of first-order propagators $P_{1 k}(\alpha)
= \exp(-\i \alpha \epsilon H_\mathrm{k} ) + O(\epsilon^2)$.  Our
strategy is to use Eqn. \ref{eq:recursive-Pj} to implement $P_{(n+1)
    z}(\xi)$ where $\xi = \hsnorm{\Omega_{n+1}}/\hsnorm{H_{z}}$.  On
the Lie algebra, this operation is represented by vector of length
$\epsilon^{n+1} \hsnorm{\Omega_{n+1}}$ oriented along the $-\i H_{z}$
axis.  Let $\upsilon$ be the rotation which performs $\upsilon
\Omega_{n+1} \upsilon^\dagger = \i \xi H_{z}$ (i.e., the operator which
rotates $\Omega_{n+1}$ onto the $\i H_{z}$ axis).  Then, by similarity
transformation under $U_T \upsilon^\dagger$
\begin{eqnarray}
U_T \upsilon^\dagger P_{(n+1) z}(\xi) \upsilon U^\dagger_T = U_T \exp( -\epsilon^{n+1} \Omega_{n+1} ) U^\dagger_T + O(\epsilon^{n+2}) = B_{n+1} ,
\label{eq:Bn}
\end{eqnarray}
the sequence $P_{(n+1) z}(\xi)$ may be transformed into precisely the
required correction sequence needed for the Solovay-Kitaev method.
There are two approaches for applying the transformation by $U_T
\upsilon^\dagger$: we may either calculate the transformed analogues
of each of the pulses in $P_{(n+1)z}(\xi)$ and then apply the
transformed pulses, or we may, when possible, directly include the
transformation pulses (or an estimate for them) as physically applied
pulses in the sequence.  The second approach is only viable when it is
possible to generate accurate inverse operations $\upsilon
U^\dagger_T$ \cite{Alway2007}.

Our construction will be complete once we have a method for generating
the simple ``pure-error'' propagator $P_{1x}(\alpha)$.  In many cases
it is sufficient to only consider $P_{1x}(\alpha)$ since the other
propagators $P_{1y}(\alpha)$ and $P_{1z}(\alpha)$ are related by a
similarity transformation.  In general, the method will depend on the
error model under consideration; here we explicitly show how to
construct this term in the amplitude and pulse-length error models and
also in the addressing error model.

\begin{figure}
\begin{center}
\includegraphics{figures/appendix/pdf/figure2}
\end{center}
\caption{Generation of the pure error term in the Solovay-Kitaev
    method. a) From Eqn. \ref{eq:recursive-Pj}, $P_{2z}(\alpha \beta)$ may
    be produced by the sequence $S_x(-\alpha) S_y(-\beta) S_x(\alpha)
    S_y(\beta)$.  On the Lie algebra the sequence corresponds to a
    closed rectangular path with an enclosed area of $\epsilon^2
    \alpha \beta$.  b) Alternatively, the rhombus construction may be
    used to generate $P_{2z}(\alpha)$ using four
    pulses. \label{fig:SK-method}}
\end{figure}

\emph{Addressing errors:} We wish to perform the evolution
$P_{1x}(\alpha)$ using a product of imperfect square-pulse propagators
$M(\theta,\phi)$.  Recall that in this model rotations on the
addressed qubit are error free $M(\theta,\phi) = R(\theta,\phi)$,
whereas on the unaddressed qubit the applied unitary depends on the
systematic addressing error $M(\theta,\phi) = R(\theta \epsilon_{N}, \phi)$.
Similarly, the sequence implementing $P_{1x}(\alpha)$ must resolve to
the identity on the addressed qubit, while on the unaddressed qubit
$\exp(-\i \alpha \epsilon_{N} H_{x}) + O(\epsilon_{N}^2)$ is applied.  This
behavior may be achieved by using a pulse sequence to implement
$P_{1x}$.  Let
\begin{eqnarray}
S_x(\alpha) = M(2 \pi a,-\phi_\alpha) M(2 \pi a, \phi_\alpha),
\label{eq:Sx}
\end{eqnarray}
where $a = \lceil |\alpha| / 4 \pi \rceil$ is a integer number of
$2\pi$ rotations and $\phi_\alpha = \arccos( \alpha / 4 \pi a )$.  On the
addressed qubit, $S_x(\alpha)$ resolves to the identity, whereas for
the unaddressed spins,
\begin{eqnarray*}
S_x(\alpha) = M(2\pi a, -\phi_\alpha) M(2\pi a, \phi_\alpha) = R(2\pi a \epsilon_{N}, -\phi_\alpha) R(2\pi a \epsilon_{N}, \phi_\alpha) = P_{1x}(\alpha),
\label{eq:Sx-sequence}
\end{eqnarray*}
is applied.  In the Lie algebraic picture, $S_x(\alpha)$ is composed
of two vectors constructed so that their vector sum is $-\i \alpha
\epsilon_{N} H_{x}$. Similarly, $S_y({\beta})= M(2\pi b,\pi/2 -\phi_\beta)
M(2\pi b, \pi/2+\phi_\beta) = P_{1y}(\beta)$.  From these basic
sequences, we may construct $P_{2z}(\alpha \beta)$ using by using the
balanced group commutator Eqn. \ref{eq:recursive-Pj}.  In figure
\ref{fig:SK-method}a we plot the sequence $P_{2z}(\alpha \beta)$ as a
vector path on the Lie algebra.  The sequence encloses a signed area
$\epsilon_{N}^2 \alpha \beta$, which is denoted by the shaded rectangular
figure.  By tuning the rotation angles $\alpha$ and $\beta$, one may
generate a term which encloses any desired area, thus allowing the
synthesis of an arbitrary pure-error term.

At this point, we discuss a subtle feature of the addressing error
model which at first appears to complicate the application of the SK
method.  Observe that on the unaddressed spin, the imperfect
propagators may only apply small rotations (i.e. rotations by angles
$\theta \epsilon_{N}$).  If we restrict ourselves to sequences composed of
resonant square pulses, then the term proportional to $P_{1z}(\alpha)$
may not be produced; we may not prepare such a term by similarity
transformation (e.g. $R(\pi/2,0) S_y(\alpha) R^\dagger(\pi/2,0)$)
since such an operation would either require a large rotation or if
instead the transformation was carried out on the individual sequence
propagators, the rotation axes would be lifted out of the $H_{x}$-$H_{y}$
plane.  Similar arguments show that the Euler decomposition is also
unavailable.

Fortunately, this restriction is not as serious as it first appears;
the SK method may be used provided that the sequence terms are chosen
with care.  We are ultimately saved by the orientation of the error
terms in the Lie algebra.  Using the BCH formula it is straightforward
to show that for sequences composed of resonant pulses, the even-order
error terms are always aligned along the $-\i H_{z}$ axis, whereas the
odd-order terms are confined to $H_{x} -H_{y}$ plane.  Likewise,
using only $S_{x}(\alpha)$ and
$S_{y}(\beta)$, it is possible to generate correction terms that
follow the same pattern.  As a consequence, in this case it is
possible to generate the correction terms $U_T \exp(-\epsilon_{N}^{n+1}
\Omega_{n+1}) U_T^\dagger$ by carefully choosing the rotation angles
and phases in the correction sequence $B_{n+1}$.

As a instructive example, we shall derive a second-order passband
sequence using the Solovay-Kitaev method.  We begin by calculating
the Magnus expansion for the seed sequence $M_{\mathrm{SK1}}(\theta,0)
= U_T \exp( \epsilon_{N}^2 \Omega_2 + \epsilon_{N}^3 \Omega_3 + \cdots )$ where
the target operation $U_T = \Id$ for the unaddressed qubit. To cancel
the second order term, we simply need to apply the inverse of $\exp(
\epsilon_{N}^2 \Omega_2) = \exp(-\i 2\pi^2 \epsilon_{N}^2 \sin(2\phi_{SK1})H_{z})$. The planar
rotation $\upsilon = \Id$,
since $B_2 = P_{2z}(-2 \pi^2 \sin(2\phi_{SK1}))$ is already oriented in the
correct direction.  One possible choice for $B_2$ is the sequence
\begin{eqnarray}
B_2 =S_x(-2\pi \cos \phi_{SK1}) S_y(2\pi \sin \phi_{SK1}) S_x(2 \pi \cos \phi_{SK1}) S_y(-2 \pi \sin \phi_{SK1}).
\end{eqnarray}
The sequence $M_\mathrm{SK2}(\theta,0) = B_2 M_\mathrm{SK1}(\theta,0)
= U_T + O(\epsilon_{N}^3)$ corrects addressing errors to second order.  We
denote an $n$th order compensating sequence produced by the
Solovay-Kitaev method using SK1 as an initial seed as SK$n$; here we
have produced an SK2 sequence.

More efficient constructions for the correction sequence $B_2$
exist. Observe that we may directly create the pure error term
$P_{2z}(\gamma)$ by using four pulses in the balanced group commutator
arrangement $ P_{2z}(\gamma) = M^\dagger (2\pi c, \phi_\gamma )
M^\dagger(2\pi c , \phi'_\gamma ) M(2\pi c, \phi_\gamma) M(2\pi c,
\phi'_\gamma)$, where $c = \lceil |\gamma| / 4\pi \rceil$ and the
phases are chosen to be $\phi_\gamma = \arcsin( \gamma / 4 \pi^2 c^2
)$ and $\phi'_\gamma = (1-\mathrm{sign}\gamma) \pi/2$.  The phase
$\phi'_\gamma$ is only necessary to ensure that the construction also
works for negative $\gamma$.
We call this arrangement the rhombus construction.  In figure
\ref{fig:SK-method}b we plot this sequence as a vector path on
$\algb{su}(2)$.  In this construction the magnitude of the error term is
tuned by adjusting the phase $\phi_\gamma$, i.e. adjusting the area
enclosed by the rhomboidal path of the sequence in the Lie algebra.
The rhombus construction has the advantage of requiring half as many
pulses as the standard method.  We will now use this construction to
produce an alternative form for SK2.  Let
\begin{eqnarray}
B_2' = M(2\pi,\phi_\gamma + \pi) M(2\pi, 0) M(2\pi,\phi_\gamma) M(2\pi, \pi)
\end{eqnarray}
where $\phi_\gamma = \arcsin( \sin(2 \phi_{SK1}) / 2 )$.  Then
$M_{\mathrm{SK2}}'(\theta,0) = B_2' M_{\mathrm{SK1}}(\theta,0) = U_T + O(\epsilon_{N}^3)$.

\emph{Amplitude / pulse-length errors:} We now turn our attention to
the compensation of amplitude and pulse-length errors.  When
considering these error models, the imperfect propagators of the form
$M(\theta,\phi) = R(\theta (1 + \epsilon_{A}),\phi)$.  In this case we may
also construct $P_{1x}$ using Eqn. \ref{eq:Sx} since the imperfect $2\pi$
rotations reduce to $M(2 \pi a, \phi) = R(2 \pi a \epsilon_{A}, \phi)$ and
$S_x(\alpha) = P_{1x}(\alpha)$.  As a consequence, if the initial seed
sequence $W_n$ is a passband sequence then $W_{n+1}$ is also a
passband sequence.  We note however, that in this error model one has
more flexibility in the synthesis of the correction sequences
$B_{n+1}$; since now the imperfect propagators apply large rotations,
then we may perform similarity transformations of sequences by
directly implementing the required pulses.  Notably, we may use
imperfect propagators of order $O(\epsilon_{A}^n)$ to apply a desired
transformation at a cost of an error of order $O(\epsilon_{A}^{n+1})$, for
example $M(\theta,\phi) P_{jz} M^\dagger(\theta,\phi) = R(\theta,\phi)
P_{jz} R^\dagger(\theta,\phi) + O(\epsilon_{A}^2)$.

The SK method may be used to calculate higher order compensation
sequences for the amplitude error model.  We begin by calculating the
relevant Magnus expansion for the seed sequence
$M_\mathrm{SK1}(\theta,0) = M(2\pi,-\phi_{SK1}) M(2\pi,\phi_{SK1}) M(\theta,0) =
U_T \exp( \epsilon_{A}^2 \Omega_2 + \epsilon_{A}^3 \Omega_3 + \cdots )$ where now
in the amplitude error model the target operation $U_T=R(\theta,0)$.
To cancel the second order term we must apply the inverse of $U_T
\exp(\epsilon_{A}^2 \Omega_2) U_T^\dagger$ = $\exp(-\i2\pi^2 \epsilon_{A}^2
\sin(2\phi_{SK1})H_{z})$.  This is precisely the same term that arose
previously for addressing errors and the error may be compensated the
same way.  In fact every $n$th order SK sequence is passband and works
for both error models.


% ************************************
% Section 4.2
% ************************************


\subsection{Wimperis / Trotter-Suzuki sequences}
\label{wimperis}
In this section we study a second family of fully compensating pulse
sequences, first discovered and applied by Wimperis
\cite{Wimperis1994}, which may be used to to correct pulse length,
amplitude, and addressing errors to second order.
These sequences have been remarkably successful and have found extensive use in
NMR and quantum information \cite{Morton2005, Xiao2006, Beavan2009}.
%, and ESR \cite{citation-needed} experiments,
%and in coherent spectroscopy of trapped atomic ions
%\cite{citation-needed}.
Furthermore, the Wimperis sequences may be
generalized to a family of arbitrarily high order sequences by
connecting them to Trotter-Suzuki formulas \cite{Brown2004}.  An
analogous composite sequence composed of rotations in $\group{SU}(4)$ may be
used to correct two-qubit operations \cite{Jones2003, Tomita2010}.  We study
the two-qubit case in section \ref{two-qubit}.

\emph{Narrowband behavior:} We begin
with the problem of identifying narrowband sequences which correct
addressing errors.  In the addressing error model operations on
addressed qubits are error free, whereas on the unaddressed qubits the
imperfect propagators takes the form $V(\vec{u}(t)) = U(\epsilon_{N} \vec{u}(t))$ and
$U_T = \Id$. Further, we shall constrain ourselves to sequences
composed of resonant square-pulse propagators.  Specifically, we
search for arrangements of four pulses that eliminate both the first
and second-order Magnus expansion term for the imperfect
propagator.  %Following the
%procedure in section \ref{method}, we use either a BCH or Magnus
%expansion for the interaction frame propagator.  In this case, we
%search for combinations of square-pulses propagators such that both
%$\epsilon \Omega_1 = 0$ and $\epsilon^2 \Omega_2 = 0$.  Then sequence
%produced will be a second-order compensating sequence.
% We begin by considering the evolution generated by the systematic
% control error.

Before explicitly describing the construction of the Wimperis
sequences, we digress shortly to point out a certain symmetry property
which may be used to ensure that $\epsilon_{N}^2 \Omega_2 = 0$.  Consider
the group product of propagators of the form,
\begin{eqnarray}
  U(\epsilon_{N} \vec{u}(t)) = U_2 U_3 U_2 U_1, \qquad U_k = \exp(-\i \epsilon_{N} t_k\vec{u}_k \cdot \vec{H}),
\label{eq:symmetry}
\end{eqnarray}
with the added condition that the first-order expansion term for
$U(\epsilon_{N}\vec{u}(t))$ has been already eliminated, i.e. $\epsilon_{N} \Omega_1 =
-\i \epsilon_{N} (t_3\vec{u}_3 + 2 t_2\vec{u}_2 + t_1\vec{u}_1) \cdot \vec{H} = 0$.  In this
arrangement, the second and fourth propagators are identical;  this pulse
symmetry along with $ t_1\vec{u}_1+2t_2\vec{u}_2+t_3\vec{u}_3= 0$ eliminates the
second-order term,
\begin{eqnarray}
\epsilon_{N}^2 \Omega_2 &=& \frac{\epsilon_{N}^2}{2} \sum_{i=1}^4 \sum_{j=1}^i [ -\i t_i \vec{u}_i \cdot \vec{H}, -\i t_j \vec{u}_j \cdot \vec{H} ]=0.
%\nonumber \\
%&=& \frac{\epsilon_{N}^2}{2} [t_1 \vec{u}_1 \cdot \vec{H} , (t_1\vec{u}_1 + 2 t_2\vec{u}_2 + t_3\vec{u}_3) \cdot \H] = 0.
% \epsilon^2 \Omega_2 &=& - \frac{\epsilon^2}{2} \sum_{k>j}^4\sum_{j=1}^4 [t_k\vec{u}_k\H,t_j\vec{u}_j\H] \\
%  &=& -\frac{\epsilon^2}{2} ([t_4\vec{u}_4\cdot\H,t_3\vec{u}_3\cdot\vec{H} ]+[t_4\vec{u}_4\cdot\H+t_3\vec{u}_3\cdot\H,t_2\vec{u}_2\cdot\vec{H} ]+[\sum_{j=2}^4 t_j\vec{u}_j\H,t_1\vec{u}_1\cdot\H]) \\
%  &=&-\frac{\epsilon^2}{2}([[t_2\vec{u}_2\cdot\H,t_3\vec{u}_3\cdot\H]+[[t_3\vec{u}_3H,t_2\vec{u}_2\cdot\H]+[-t_1\vec{u}_1\cdot\H,t_1\vec{u}_1\cdot\H])\\
%  &=& 0.
\label{eq:symmetry-cancel}
\end{eqnarray}
As a consequence, $U(\epsilon_{N} \vec{u}(t)) = \Id + O(\epsilon_{N}^3)$.
 %We note
%that the cancellation of the second-order term is purely a result of
%the symmetry of the sequence.
Alternatively, one may regard the
product $T = U_2U_3U_2$ as a second-order symmetric Trotter-Suzuki
formula for the inverse operation $U_1^\dagger$ \cite{Suzuki1992}
which approximately cancels the undesired rotation $U_1$. Considering
the control fields applied during the application of the corrector
sequence $T$, the applied control Hamiltonian is symmetric with
respect to time inversion.  By a well known theorem, all even-order
expansion terms produced by a time-symmetric Hamiltonian cancel
\cite{Burum1981, Levitt2008} (i.e. $\epsilon_{N}^{2j} \Omega_{2j} = 0$,
for all positive integers $j$).  Thus by implementing symmetric
corrector sequences $T_{2j}$, it is sufficient to only consider the
cancellation of the remaining odd-order error terms.  In section
\ref{arbitrarily-accurate-TS} we will inductively develop a series of
corrector sequences of increasing accuracy based on symmetric
Trotter-Suzuki formulas \cite{Suzuki1992, Brown2004}.

The cancellation of the second-order term may also be inferred from
geometric considerations on the Lie algebra.  To be concrete, consider
a sequence of the form Eqn. \ref{eq:symmetry} where
\begin{eqnarray}
\epsilon_{N} t_{1}\vec{u}_1\cdot\vec{H} &=& \theta \epsilon_{N} H_{x} \nonumber \\
\epsilon_{N} t_{2}\vec{u}_2\cdot\vec{H} &=& \pi \epsilon_{N}( \cos\phi_{N2} H_{x} + \sin \phi_{N2} H_{y} ) \nonumber \\
\epsilon_{N} t_{3}\vec{u}_3\cdot\vec{H} &=& 2 \pi \epsilon_{N}( \cos\phi_{N2} H_{x} - \sin \phi_{N2} H_{y} ),
\label{eq:nb1-vectors}
\end{eqnarray}
and the phase is $\phi_{N2} = \arccos(-\theta/4\pi)$ is chosen so that $
t_1\vec{u}_1+2t_2\vec{u}_2+t_3\vec{u}_3= 0$, i.e., the vectors form a closed path on
the dynamical Lie algebra.  Figure \ref{figure-wimperis}a is a diagram
of these vectors on $\algb{su}(2)$.  In $\algb{su}(2)$, the Lie bracket is
equivalent to the vector cross product \cite{Gilmore}, therefore we
may interpret $\hsnorm{\epsilon_{N}^2 \Omega_2}$ as the signed area enclosed
by the vector path on the Lie algebra.  We note that the sequence
under study encloses two regions of equal area and opposite sign,
ensuring that the second order term is eliminated.

\begin{figure}
\begin{center}
%\includegraphics{figures/chapter5/pdf/figure3}
\end{center}
\caption{a) Vector path followed by N2 on the Lie algebra. b)
    Trajectory of an unaddressed spin during an N2 sequence, using
    imperfect rotations of the form $M(\theta,\phi) =
    R(\theta\epsilon_{A},\phi)$.  c) B2 correcting an amplitude error, using
    imperfect rotations of the form $M(\theta, \phi) =
    R(\theta(1+\epsilon_{A}),\phi)$.  In these plots $\epsilon_{N} = \epsilon_{A} =
    0.2$.
   \label{figure-wimperis}}
\end{figure}

With this insight, the construction of a second-order narrowband
sequence is straightforward.  The vectors Eqn. \ref{eq:nb1-vectors}
correspond to the sequence
\begin{eqnarray}
  M_\mathrm{N2}(\theta,0) = M(\pi,\phi_{N2}) M(2\pi,-\phi_{N2}) M(\pi,\phi_{N2}) M(\theta, 0).
\label{eq:N2}
\end{eqnarray}
Wimperis refers to this sequence as NB1, and indeed, this is the
established name in the literature.  In the current article, we label
this sequence N2 in anticipation of the generalization of this form to
N2$j$, which compensates addressing errors to $O(2j)$.  We use this
language to avoid confusion with other established sequences, namely
NB2, NB3, etc \cite{Wimperis1994}.  The N2 sequence may be used to
compensate addressing errors.  For unaddressed qubits $M(\theta,\phi)
= R(\theta \epsilon_{N},\phi)$ and thus from Eqn. \ref{eq:symmetry-cancel} and
Eqn. \ref{eq:nb1-vectors} it follows that $M_{\mathrm{N2}}(\theta,0) = \Id
+ O(\epsilon_{N}^3)$.  Thus on unaddressed qubits the sequence performs the
identity operation up to second-order.  Furthermore, for addressed
qubits $M(\theta,\phi) = R(\theta,\phi)$ and therefore
$M_{\mathrm{N2}}(\theta,0) = R(\theta,0)$.  As a result, when the
sequence $M_\mathrm{N2}(\theta,0)$ is used in the place of the
imperfect operation $M(\theta,0)$, the discrimination between
addressed and unaddressed spins is enhanced.  In figure
\ref{figure-wimperis}b we plot the magnetization trajectory for an
unaddressed qubit under an N2 sequence.

\emph{Broadband behavior:} As previously discussed, broadband
sequences are best suited for correcting amplitude or pulse-length
errors.  In the following we will explicitly consider the amplitude
error model, where $M(\theta,\phi) = R(\theta(1 + \epsilon_{A}),\phi) =
R(\theta,\phi) R(\theta \epsilon_{A},\phi)$.  Although a pulse sequence may
be studied by considering the interaction frame propagator as
described in section \ref{method}, the method originally used by
Wimperis is simpler.  Wimperis' insight was that for lowest orders the
toggled frame could be derived geometrically, using the relation
$R(\theta,-\phi)= R(\pi,\phi) R(\theta,3\phi) R(\pi,\phi+\pi)$.
Consider the application of the target gate $U_T = R(\theta,0)$ using
the sequence
\begin{eqnarray}
M_\mathrm{B2}(\theta,0) = M(\pi,\phi_{B2}) M(2\pi,3\phi_{B2}) M(\pi,\phi_{B2}) M(\theta, 0)
\end{eqnarray}
where $\phi_{B2} = \arccos(-\theta/4\pi)$.  This is the Wimperis
broadband sequence, traditionally called BB1 but here denoted as B2.
When rewritten in terms of proper rotations one obtains,
\begin{eqnarray*}
  M_\mathrm{B2}(\theta,0) &=&  R(\pi \epsilon_{A}, \phi_{B2}) \Big( R(\pi,\phi_{B2}) R(2\pi \epsilon_{A}, \phi_{B2}) R^\dagger(\pi, \phi_{B2}) \Big) R(\pi \epsilon_{A}, \phi_{B2} ) R(\theta \epsilon_{A} , 0) R(\theta, 0) \nonumber \\
%M(\pi,\psk) M(2\pi,3\psk) M(\pi,\psk) M(\theta, 0)\\
%&=&M(\pi,\phi) M(2\pi,3\psk) R(\pi,\psk)R(\pi\epsilon,\psk)R(\theta\epsilon,0) R(\theta, 0)\\
%&=&M(\pi,\phi) R(\pi,\psk)M(2\pi,-\psk) R(\pi\epsilon,\psk)R(\theta\epsilon,0) R(\theta, 0)\\
&=& \Big[ R(\pi \epsilon_{A}, \phi_{B2}) R(2\pi \epsilon_{A}, -\phi_{B2}) R(\pi \epsilon_{A}, \phi_{B2}) R(\theta \epsilon_{A}, 0) \Big] R(\theta,0)
\end{eqnarray*}
where the identity $R(2\pi,3\phi_{B2}) = -\Id = R(\pi,\phi_{B2} + \pi)
R(\pi,\phi_{B2} + \pi)$ was used.  Let $Q$ denote the quantity enclosed in
square brackets, so that we may write $M_{\mathrm{B2}}(\theta,0) = Q
U_T$.  Observe that $Q$ is precisely the form considered previously in
N2.  From our previous result we may conclude $Q = \Id + O(\epsilon_{A}^3)$
and $M_{\mathrm{B2}}(\theta,0) = R(\theta,0) + O(\epsilon_{A}^3)$.  As a
result, when $M_\mathrm{B2}(\theta,0)$ is used in the place of the
imperfect operation $M(\theta,0)$ the effect of the systematic
amplitude error is reduced.  Note that errors for the B2 and N2
sequences follow equivalent paths on the Lie algebra in their
respective interaction frames.  In figure \ref{figure-wimperis}c we
plot the magnetization trajectory for a qubit under a B2 sequence with
amplitude errors.

\emph{Passband behavior:} In some cases, it is convenient to have a
passband pulse sequence that corrects for both addressing errors and
for amplitude errors, as we saw with the Solovay-Kitaev sequences.
The passband Wimperis sequence P2 is simply two Solovay-Kitaev
correction sequences in a row where the order of pulses is switched,
$M_\mathrm{P2}(\theta,0) = M(2\pi,\phi_{P2}) M(2\pi,-\phi_{P2})
M(2\pi,-\phi_{P2}) M(2\pi,\phi_{P2}) M(\theta,0)$, with $\phi_{P2}=
\arccos(-\theta/8\pi)$.  The switching of pulse order naturally
removes the second order error term.  One may verify that this
sequence works for both addressing and amplitude errors.


% ************************************
% Section 4.2.1
% ************************************


\subsubsection{Arbitrarily accurate Trotter-Suzuki sequences}
\label{arbitrarily-accurate-TS}
The Wimperis sequences rely on a certain symmetrical ordering of pulse
sequence propagators to ensure that even order Magnus expansion terms
are eliminated.  We may further improve the performance of these
sequences by taking advantage of additional symmetries that cancel
higher-order terms.  In the following, we shall show how by using
symmetric Trotter-Suzuki formulas \cite{Trotter1958, Suzuki1993,
  Suzuki1992} a family of arbitrarily accurate composite pulse
sequences may be constructed \cite{Brown2004, Alway2007}.  The Lie
algebraic picture along with the Magnus and BCH series will be
important tools in this process.

\emph{Symmetrized Suzuki formulas:} Before discussing the particular form of
these sequences, it is helpful to briefly mention a few important results
regarding symmetric products of time-independent propagators
\cite{Suzuki1992}.  Given a series of skew-Hermitian time-independent
Hamiltonians $\{ \tilde{H}_1, \tilde{H}_2, \dots , \tilde{H}_m \}$ such that $\sum_i^m
\tilde{H}_i = \tilde{H}_T$, the BCH expansion tells us
\begin{eqnarray}
\prod_{i=1}^{m} \exp(\lambda \tilde{H}_i) = \exp \left( \lambda \tilde{H}_T + \sum_{n=2}^\infty \lambda^n \Omega_n \right),
\end{eqnarray}
where $\lambda$ is a real parameter and the expansion terms $\Omega_n$
depend on the specific ordering of the sequence.  If we choose to
apply the operators in a time-symmetric manner such that $\exp(\lambda
\tilde{H}_i ) = \exp( \lambda \tilde{H}_{m+1-i} )$, then the symmetry of the pulse
removes all even-order terms.  For symmetric products, we have
\begin{eqnarray}
\prod_{symmetric} \exp(\lambda \tilde{H}_i) = \exp \left( \lambda \tilde{H}_T + \sum_{j=2}^\infty \lambda^{2j-1} \Omega_{2j-1} \right).
\end{eqnarray}

An important observation concerning the elimination of the remaining
odd terms was made by Suzuki \cite{Suzuki1992}.  Provided that $\tilde{H}_i
= p_i \tilde{H}_T + (p_i)^{2j-1}\tilde{H}_B$, where the coefficients $p_i$ are
real numbers, there exist certain choices of coefficients such that
$\sum_i^m \tilde{H}_i = \tilde{H}_T$.  This requires that $\sum_i^m p_i=1$ and
$\sum_i^m (p_i)^{2j-1}=0$.  The situation is considerably simplified
if we restrict ourselves to sequences composed of just two kinds of
propagators, $U_1 = \exp(\tilde{H}_1)$ and $U_2 = \exp(\tilde{H}_2)$.  In this case
the previous expression simplifies to $n_1 p_1
+ n_2 p_2 = 1$ and $n_1 p_1^{2j-1} +n_2 p_2^{2j-1} = 0$, where the
integers $n_1$ and $n_2$ are the number of $U_1$ and $U_2$ pulses
required to produce a sequence that is independent of $H_B$ up to
$O(p^{2j+1})$.  We solve for a set of coefficients by setting
$p_2 = -2p_1$ and $n_2 = 1$, thus yielding $n_1 = 2^{2j-1}$ and $p_1=1/(2^{2j-1}-2)$.

Combining these observations, if $W_{2j-2}(p)$ is a $(2j-2)$th
approximation of $\exp( p \tilde{H}_T )$ and $\Omega_{2j-1}=p^{2j-1} \tilde{H}_B$
where $\tilde{H}_B$ is independent of $p$, then we can construct $W_{2j}(1)$
from the lower approximations, $W_{2j}(1)= (W_{2j-2}(p))^{2^{2j-2}}
W_{2j-2}(-2p)(W_{2j-2}(p))^{2^{2j-2}}$. % This is quite remarkable
%since the construction only depends on the scaling of the remaining
%terms with $p$, and does not require the direct computation of the
As a result, Suzuki formulas provide a path of producing higher-order
sequences from a symmetric combination of lower-order sequences.
Notice that the symmetric decomposition is used to keep the even-order
terms zero.  % We note
% that given the freedom in the choice of the parameters $p_i$ this
% simple construction is likely inefficient;  however  the
% compensating sequences we construct require that
% the effective $p_i$'s must be integer multiples
% of each other.

\emph{Passband behavior:} In the following we will seek to generalize
the second-order passband sequence P2 to an arbitrarily accurate
passband sequence P$2j$.  Our goal is to develop a correction sequence
$T_{2j}(k,\phi) = \exp( \i \theta \epsilon_{A} H_{x} ) + O(\epsilon_{A}^{2j + 1})$
that cancels the unwanted rotation of a $M(\theta,0)$ operation.

When considering passband sequences, it is convenient to use rotation
angles that are integer multiples of 2$\pi$.  Let us define the
triangular motif,
\begin{eqnarray}
  T_1(k,\phi)&=& M(2 k \pi, -\phi) M(2 k \pi, \phi ) \nonumber \\
  %&=& R(2k\pi\epsilon_{A}, -\phi)R(2k\pi\epsilon_{A}, \phi) \nonumber \\
  &=&\exp(-\i 4k \pi \epsilon_{A} \cos \phi H_{x} -\i (2\pi k\epsilon_{A})^2 \cos \phi \sin \phi H_{z} ) + O(\epsilon_{A}^3).
\end{eqnarray}
Observe the passband sequence SK1 may now be written as
$M_{\mathrm{SK}1}(\theta,0) = T_1(1,\phi_{SK1}) M(\theta,0)$ and where
the phase $\phi_{SK1}$ is as defined previously.
\begin{figure}
\begin{center}
\includegraphics{figures/appendix/pdf/figure4}
\end{center}
\caption{Vector path followed by P4 on the Lie algebra.
   \label{figure-P4}}
\end{figure}
The remaining second-order term is odd with respect to $\phi$, and is
related to the vector cross product on the Lie algebra.  A
second-order sequence may be constructed by combining two
$T_1(k,\phi)$ terms so that the correction sequence is symmetric and
the cross product cancels. Let us define the symmetrized product
\begin{eqnarray}
T_2(k,\phi)&=&T_1(k,-\phi)T_1(k,\phi) \nonumber \\
&=& \exp( p \tilde{H}_T + p^3 \tilde{H}_B ) + O(p^5),
\end{eqnarray}
where the length $p = - (8 k \pi/ \theta) \cos \phi $, the target
Hamiltonian $\tilde{H}_T = \i \theta \epsilon_{A} H_{x}$, and $\tilde{H}_B = \epsilon_{A}^3
\Omega_3 / p^3$ is the remaining term we wish to cancel.  $\Omega_3$
is a function that depends on $k$ and $\phi$ such that $\tilde{H}_B$ is a
function of $\phi$ but not of the length scale $k$.  For fixed $\phi$
and variable $k$, this makes $H_B$ independent of $p$. Observe that
the passband sequence P2 may now be written as $M_{\mathrm{P}2}
(\theta, 0) = T_2(1,\phi_{P2}) M(\theta,0)$, where again $\phi_{P2} =
\arccos( - \theta / 8 \pi )$.

Our strategy is to use a Suzuki formula to construct higher-order
$T_{4}(k,\phi)$ (that is, $T_{2j}(k,\phi)$ for $j = 2$) using a
symmetric combination of $(n_1 = 8)$-many $\exp(\tilde{H}_1) = T_2(k,\phi)$
sequences and a single $\exp(\tilde{H}_2) = T_2(-2k,\phi)$ sequence,
yielding
\begin{eqnarray}
T_4(k,\phi) = \Big(T_2(k,\phi) \Big)^4T_2(-2k,\phi) \Big(T_2(k,\phi) \Big)^4.
\end{eqnarray}
In order to produce to required correction term, the parameters
$(k,\phi)$ must be chosen such that $(n_1 - 2)p = 1$.  The
fourth-order passband sequence P4 is $M_{\mathrm{P4}}(\theta,0) =
T_4(1, \phi_{P4}) M(\theta,0)$, where $\phi_{P4} = \arccos( -\theta /48 \pi
)$.  In figure \ref{figure-P4} we plot the vector path followed by P4
on the Lie algebra.  This result may be further generalized.  To
produce $T_{2j}(k,\phi)$ requires $(n_1 = 2^{2j - 1})$-many $T_{2j -
    2}(k, \phi)$ sequences and a single $T_{2j - 2}(-2k, \phi)$
sequence in the symmetric ordering,
\begin{eqnarray}
T_{2j}(k,\phi)= \Big(T_{2j-2}(k,\phi) \Big)^{2^{2j-2}} T_{2j-2}(-2k,\phi) \Big( T_{2j-2}(k,\phi) \Big)^{2^{2j-2}}.
\end{eqnarray}
We then fix $\phi$ so that the first-order term cancels the unwanted
rotation, yielding
\begin{eqnarray}
\phi_{\mathrm{P2}j} = \arccos \left( - \frac{\theta}{2\pi f_j} \right)
\end{eqnarray}
where $f_j = (2^{2j - 1} -1) f_{j-1}$ and for the sequence P2$j$, $f_1
= 4$.  Then the $2j$th-order passband sequence P2$j$ is
$M_{\mathrm{P}2j} (\theta,0) = T_{2j} ( 1, \phi_{\mathrm{P}2j})
M(\theta,0)$.

% All the even order terms vanish due to symmetry and for
% this sequence the $\ell$th order Magnus term $\Omega_\ell = k^\ell
% C^{T_2}_\ell(\phi)$, where $C^{T_2}_\ell (\phi)$ is an anti-Hermitian
% operator that depends on the pulse sequence, $\phi$, and the order
% $\ell$, but not on $k$ or $\epsilon_{A}$.  We also note that $k$ serves as a
% length scale. Using the sign and length, we can cancel the third order
% term by taking a twice as large $T_2$ step backwards, $T_2(-2k,\phi)$,
% for every eight $T_2(k,\phi)$ steps forward. In order to remove all
% even order terms, we do this in a symmetric way: four steps forward,
% one double-size step backwards, four steps forward.
% \begin{eqnarray}
% T_4(k,\phi)&=&\Big(T_2(k,\phi) \Big)^4T_2(-2k,\phi) \Big(T_2(k,\phi) \Big)^4 \nonumber \\
% &=&\exp( -\i 24 k \pi \epsilon_{A} \cos \phi H_{x} + C^{T4}_{5}(\phi)k^5\epsilon_{A}^5+ \dots )
% \end{eqnarray}
% We can then define $M_{\mathrm{P}4}(\theta,0) = T_4(\psk) M(\theta,0)$
% where $\psk = \arccos(-\theta/48\pi)$, which is a passband sequence
% which corrects to fifth order.

The same method can be used to develop exclusively narrowband or
broadband sequences called N2$j$ and B2$j$ respectively
\cite{Brown2004}.  This requires redefining the bottom recursion layer
$T_2(k,\phi)$ to have either narrowband or broadband properties. For
N2$j$, $f_1 = 2$ and $T_2(k,\phi) = T_1(k/2,\phi)T_1(k/2,-\phi)$. For
B2$j$ $f_1 = 2$, but $T_2$ is slightly more complicated; when $k$ is
even $T_2(k,\phi) = T_1(k/2,-\phi)T_1(k/2,\phi)$ just like N2$j$,
however when $k$ is odd and $T_2(k,\phi) =
M(k\pi,\phi)M(k\pi,3\phi)M(k\pi,3\phi)$.  In figure
\ref{figure-B2j-P2j}, we compare several of the generalized
Trotter-Suzuki sequences to the ideal unitaries $U_T = R(\pi/2,0)$ in
the case of amplitude errors (top row) and $U_T = \Id$ in the case of
addressing errors on unaddressed qubits (bottom row).

\begin{figure}
\begin{center}
\includegraphics{figures/appendix/pdf/figure5}
\end{center}
\caption{Infidelity of the Trotter-Suzuki sequences B2$j$, P2$j$, and
    N2$j$.  In the amplitude error model (top row) $M(\theta,\phi) =
    M(\theta(1+\epsilon_{A}),\phi)$ and $U_T = R(\pi/2,0)$.  In the
    addressing error model (bottom row) on the unaddressed qubits
    $M(\theta,\phi) = M(\theta \epsilon_{N},\phi)$ and $U_T = \Id$, while on
    the addressed spins $R(\pi/2,0)$ is applied.  Each error model
    establishes a separate preferred interaction frame; when
    transformed into the appropriate pictures, the B2$j$ and N2$j$
    sequences are homologous.  The passband sequences P2$j$ can
    correct both amplitude and addressing errors at the cost of
    reduced efficacy.
  \label{figure-B2j-P2j}}
\end{figure}

