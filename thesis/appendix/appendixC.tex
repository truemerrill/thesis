\chapter{Improved state-detection using Bayesian inferrence} 

\section{Single detection events}

\begin{definition}
  An \emph{experiment} $E_j$ measures the state of the system by producing an \emph{outcome} $o_j \in O$ among the admissible set of outcomes $O$.  An outcome may be used to infer information from the state of the system. 
\end{definition}

In the context of quantum measurement in ion-trap quantum processing, individual experiments $E_j$ refer to the process of initializing, manipulating, and detecting a qubit register; whereas an appropriate outcome would be some integer number of photon counts registered by the PMT over a collection interval. Henceforth, we reserve the symbol $n_j$ to represent the number of collected photons during experiment $E_j$.  

In quantum mechanics the outcomes of experiments are rarely known with certainty, rather we say the outcomes occur with some probability $P(n_j)$.  From the collapse postulate it is clear that the probability of observing an outcome is affected by the state of system prior to measurement.  Namely, information regarding the quantum state conditions the frequency of experimental outcomes.  For example the conditional probability $P(n_j|S_i)$ represents the likelihood of detecting $n_j$ photons, provided that the system was prepared in the pure state $\rho(S_i) = |S_i\rangle\langle S_i|$ prior to detection.  

\begin{remark}
The conditional probabilities $P(n_j|S_i)$ are related to a photon collection histogram $H_i$ for a qubit prepared in the state $\rho(S_i)$.  Suppose a collection of $N$ experiments are performed on $\rho(S_i)$ with the results binned into a histogram by outcome.  The $n_j$-th histogram bin is $H_i(n_j) = N P(n_j|S_i)$.
\end{remark}

%Suppose $|S_i\rangle$ represents a bright state (which scatters many photons) while $|S_k\rangle$ represents a dark state.  Clearly $P(n_j|S_i) \neq P(n_j|S_k)$ in most cases since these states scatter different numbers of photons.  If we chose an outcome $n_j$ that was equally probable for both state preparations, then surely there exists an alternate outcome $n_\ell$ such that $P(n_{\ell}|S_i) \neq P(n_\ell|S_k)$.

The qubit could be prepared in an admixture of measurement basis states.  For instance the system could be prepared in a mixed state $\rho$ with each component $|S_i\rangle$ being detected with probability $P(S_i) = \langle S_i| \rho |S_i\rangle$.  Probabilities regarding the occurrence of each state $P(S_i)$ and each outcome $P(n_j)$ are related by the conditional distributions $P(n_j|S_i)$ and $P(S_i|n_j)$.  The law of total probability states,
\begin{eqnarray}
  P(n_j) = \sum_i P(n_j|S_i) P(S_i) \qquad \textrm{and} \qquad
  P(S_i) = \sum_j P(S_i|n_j) P(n_j).
  \label{total_prob}
\end{eqnarray}
Thus to infer between probabilities requires information about the conditional probabilities of states and experimental outcomes.

We are now prepared to introduce a method for state detection which uses \emph{Bayesian inference} to determine the population inversion on a trapped-ion qubit.  Without loss of generality, the density matrix of an arbitrary qubit state may be written as
\begin{eqnarray}
\rho = \begin{pmatrix} p & \alpha \\ \alpha^* & 1-p \end{pmatrix},
\end{eqnarray}
where by the Hermiticity requirement $\rho = \rho^\dagger$ it is evident that $p$ is a real parameter (representing the population inversion) and $\alpha$ is a complex number (representing the coherences).  We note that projection onto the measurement basis is independent of $\alpha$ since $\langle Z \rangle = \mathrm{tr}(\rho Z) = 2p - 1$. For the purposes of state detection, we may consider all states which differ only by the parameter $\alpha$ to be equivalent, since they all yield the same expectation value.  We denote these states as $\rho(p)$.

An ion prepared in state $\rho(p)$ will scatter photons when interacting with a resonant laser field.  The probability of collecting $n_j$ photons from an ion in state $\rho(p)$ is represented by
\begin{eqnarray}
P(n_j|p) = (1-p) P(n_j|S_0) + p P(n_j|S_1),
\label{P(n|p)}
\end{eqnarray}
where $P(n_j|S_i)$, $i \in \{ 0, 1\}$ represent the probability of collecting $n_j$ photons from the qubit basis states $|0\rangle$ and $|1\rangle$. Equation \ref{P(n|p)} allows us to compute the probability of the outcome $n_j$ provided that the population $p$ is known.  We instead require a method of estimating the population $p$ from a known experimental outcome $n_j$.  The inversion is accomplished using Bayes' theorem,
\begin{eqnarray}
  P(p|n_j) = \frac{P(n_j|p) P(p)}{P(n_j)}.
\end{eqnarray} 
Substituting equation \ref{P(n|p)} yields,
\begin{eqnarray}
  P(p|n_j) = \frac{\{(1-p) P(n_j|S_0) + p P(n_j|S_1)\} P(p)}{P(n_j)}.
  \label{P(p|n_j)_derive}
\end{eqnarray}

The probabilities $P(p)$ and $P(n_j)$ represent our \emph{prior} knowledge of the inversion parameter $p$ and the outcome $n_j$.  Prior to any measurement, we lack any knowledge of $p$; any inversion $p \in [0,1]$ is equally likely.  Further since $p$ is a continuous parameter, $P(p)$ should be a probability distribution function which satisfies the normalization condition,
\begin{eqnarray}
  \int_0^1 dp P(p) = 1.
\end{eqnarray}
These conditions therefore require $P(p) = 1$, which is a continuous, structureless probability distribution function over the domain $p \in [0,1]$.

The probability $P(n_j)$ may also be determined from our prior knowledge of the system.  From the continuous analogue of the law of total probability (e.g. equation \ref{total_prob}), $P(n_j)$ may be represented as
\begin{eqnarray}
  P(n_j) = \int_0^1 dp P(n_j|p) P(p) = \frac{P(n_j|S_0) + P(n_j|S_1)}{2}.
  \label{P(n_j)}
\end{eqnarray}
Finally inserting equation \ref{P(n_j)} and $P(p) = 1$ into equation \ref{P(p|n_j)_derive} produces the result,
\begin{eqnarray}
  P(p|n_j) = 2 \frac{(1-p)P(n_j|S_0) + pP(n_j|S_1)}{P(n_j|S_0) + P(n_j|S_1)}.
  \label{P(p|n)}
\end{eqnarray}
This result may be interpreted as follows.  Before a detection event, an experimenter lacks any information to infer the population $p$; the probability function $P(p)$ is structureless.  However after observing the outcome $n_j$, an experimenter may update the inferred distribution to $P(p|n_j)$.  The optimal estimation for $p$ occurs when $P(p|n_j)$ is maximized; in this case the distribution is linear in $p$ and the maximum over the domain occurs either at $p = 0$ or $p = 1$.  Thus the information gained from a single detection event has improved the distinguishability between the qubit states.

The error associated with this estimation may be computed as,
\begin{eqnarray}
error
\end{eqnarray}

\begin{remark}
The Bayesian inference used in equation \ref{P(p|n)} assumes the probabilities $P(n_j|S_i)$ are known.  In practice, these quantities are estimated by experimentally measuring histograms $H_0(n_j) = N P(n_j|S_0)$ and $H_1(n_j) = N P(n_j|S_1)$, constructed from $N$ independent detection events.  The reader should be aware of an additional error related to the finite sampling of these quantities.
\end{remark}


\section{Multiple detection events}
An estimation for an inversion $p$ may be further refined by including additional detection events to the inferred distribution.  Let $\vec{n'} = \{n_{N-1}, \cdots, n_j, \cdots, n_1 \}$ represent a set of outcomes from $N-1$ independent experiments.  Suppose that after these measurements, we have arrived at the distribution $P(p|\vec{n'})$.  After performing an $N$th experiment with outcome $n_N$, we construct $\vec{n} = \{n_N, \vec{n'}\}$ and the probability distribution,
\begin{eqnarray}
P(p|\vec{n}) = \frac{P(n_N|p) P(p|\vec{n'})}{P(n_N)},
\end{eqnarray}
where $P(n_N) = (P(n_N|S_0) + P(n_N|S_1))/2$.  This implies the recursive construction,
\begin{eqnarray}
P(p|\vec{n}) = \prod_{j=1}^N \frac{P(n_j|p)}{P(n_j)} P(p) = \prod_{j=1}^N P(p|n_j).
\label{P(p|vn)}
\end{eqnarray}
Observe that as the distribution $P(p|n_j)$ is linear in $p$, the distribution $P(p|\vec{n})$ is an $N$th order polynomial in $p$.  Again the optimal estimate for $p$ is that where $P(p|\vec{n})$ is maximized.

An alternative representation for equation \ref{P(p|vn)} is possible.  Consider two experiments $E_j$ and $E_k$ that produce the same outcome $n_j = n_k$.  Then $P(p|n_j) = P(p|n_k)$ and their product can be represented as $P(p|n_j)^2$.  Likewise, suppose we binned experimental results by outcome into a histogram $H_p$. Since each outcome $n_j$ occurs $H_p(n_j)$ times equation \ref{P(p|vn)} may be rewritten as,
\begin{eqnarray}
P(p|\vec{n}) = \prod_{n_j=1}^N P(p|n_j)^{H_p(n_j)}.
\end{eqnarray}
We can then conclude that the population $p$ may be estimated by measuring three photon collection histograms: the histograms $H_0$ and $H_1$ for an ions prepared in the dark state $\rho(S_0)$ and bright state $\rho(S_1)$ respectively, and a third histogram $H_p$ for constructed from preparations of the state $\rho(p)$.  
%We introduce the state $|p\rangle = (1 - p)|0\rangle + p|1\rangle$.
