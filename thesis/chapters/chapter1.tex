\chapter{Introduction}

Recent years have witnessed tremendous progress in the power, scale, and pervasiveness of computers.  Many modern devices now incorporate computing elements, and already computing plays a critical role in %business and finance, agriculture and logistics, security and defense, media and entertainment, engineering and science, and other 
many aspects of modern society.  Yet despite their widespread availability and usefulness, computers are currently unable to efficiently solve certain kinds of problems, such as breaking an RSA encryption by factoring keys~\cite{Boneh1999}, or solving a full configuration-interaction (full CI) problem in quantum chemistry~\cite{Szabo1989}.  These problems are difficult from an algorithmic perspective; although a solution can be quickly verified, no efficient methods exist to compute a solution in the first place.  

Quantum computing is a rapidly emerging technology that may allow these problems to be solved efficiently.  Quantum computers differ from more common (classical) computers in that they exploit quantum phenomena (such as superposition, interference, and entanglement) to process data.  These properties enable more efficient algorithms.  Quantum computers were first conceived in the 1980's by Richard Feynmann~\cite{Feynman1982}, Paul Benioff~\cite{Benioff1980, Benioff1982} and others, who mostly focused on using these devices to simulate other quantum systems.  The 1990's saw important advances in quantum algorithms~\cite{Deutsch1992, Shor1994, Grover1997} and theoretical quantum computing~\cite{Bennett2000, DawsonQIC2006}.  Notably, Peter Shor discovered a factoring algorithm~\cite{Shor1994} which operates in polynomial time, which greatly improved on the best known classical algorithms. % which operates in superpolynomial time.
These advances stimulated the development of quantum computing devices~\cite{Imamog1999,Brennen1999,Chiorescu2003,Clarke2008,LyonNature2008,Neumann2008}, a line of research which continues to the present day.  

Currently multiple quantum computing technologies are under development.  Computers constructed from trapped atomic ions are among the most promising.  Early work by Wolfgang Paul developed the Paul trap~\cite{Paul1953, Paul1990}.  Hans Dehmelt and others advanced laser cooling and ion imaging techniques~\cite{Neuhauser1980}.  These elements were combined in a proposal by Ignacio Cirac and Peter Zoller~\cite{Cirac1995}, who first described how interactions between trapped ions could be used for computation.  More recent progress by a number of researchers (notably by Dave Wineland, Rainer Blatt, and others) have demonstrated all of the essential technologies required for universal quantum computation.  Current research focuses on improving the quality of quantum logic operations and integrating existing technologies into a computer architecture amenable to scaling to large numbers of trapped ions~\cite{Kielpinski2002, Kim-QIC.5.515.2005}.

\begin{figure}
\begin{center}
\includegraphics{figures/chapter1/pdf/figure2}
\end{center}
\caption[Developments towards an ion-trap quantum computer]{
	Developments towards an ion-trap quantum computer.  Wolfgang Paul developed the Paul trap in 1953~\cite{Paul1953, Paul1990}.  Doppler cooling~\cite{Neuhauser1980} and sideband cooling~\cite{Diedrich1989} techniques were applied to trapped ions in the 1980's.  Cirac and Zoller proposed combining these elements in a quantum computer~\cite{Cirac1995}.  Soon afterwords the first two-qubit gates were achieved~\cite{Monroe1995, Turchette1998}.  Recently, quantum teleportation~\cite{Barrett2004}, quantum error correction~\cite{Chiaverini2004}, and microfabricated surface-electrode traps~\cite{Seidelin2006} have been demonstrated.  The current record for the largest entangled state is 14 ions~\cite{Monz2011}.
\label{fig:timeline}}
\end{figure}

One technology well-suited for scaling is the surface-electrode trap~\cite{Chiaverini2005, Seidelin2006}.  These devices place trapping electrodes in a common plane and are easily miniaturized using microfabrication techniques.  Furthermore, complex electrode geometries may be produced, for instance geometries compatible with ion transport~\cite{Blakestad2009, Doret2012} and ion reordering~\cite{Hensinger2006, Wright2013}.  Here at Georgia Tech and the Georgia Tech Research Institute (GTRI) we have built a research program that develops complex surface-electrode traps.  A major focus of this thesis is the description and testing of these devices.

%The first proposal for an ion-trap quantum computer Later Ignacio Cirac and Peter Zoller realized.  Important progress has been achieved by Dave Wineland and Rainer Blatt.
%Now computers are used in many aspects of our lives.  Despite the massive increase in available computing power, there still are certain problems that are very difficult for computers to solve.  For instance factoring a large number or a Full configuration interaction \emph{ab-initio} calculation. 
%Quantum computation is an emerging technology that could revolutionize computer science, cryptography, algorithms, and also could solve difficult problems in chemistry and physics.

\section{Why errors ruin the party}
%In the last century two computing technologies were common.  
%Early in the history of computing, two competing technologies battled for supremacy of the computing landscape.  
%Analog computers processed data using continuous voltages, however, were sensitive to noise and other errors.  Although analog signals can be stabilized via measurement and feedback control, in practice the accuracy of analog computers was limited by the quality of their internal components.  Eventually, analog computers were replaced by modern digital computers which store data in discrete bits and are therefore less sensitive to error.  

During logic operations quantum computers utilize a continuum of superposition states.  If the system is subjected to a noise process or some kind of systematic error, the computer tends to produce an incorrect superposition.  Overcoming errors is a major challenge in quantum computing, and a serious obstacle towards building practical devices~\cite{Nielsen2000}.  A common strategy in classical systems is to use measurement and feedback control to stabilize a noisy system.  This strategy is difficult in quantum systems since measurement itself irreversibly alters the quantum state.%, although recent progress has been made using weak measurements~\cite{Lloyd2000,Sayrin2011}.

Quantum error correction (QEC) is a method of protecting quantum information from noise and decoherence by identifying errors and employing active correction measures~\cite{Shor1995, Gottesman2010}.  These methods are expected to be an essential tool in future quantum processors, since they can correct arbitrary errors provided that they occur with a sufficiently low probability~\cite{Preskill1998} (called the error threshold).  QEC employs a kind of quantum redundancy, in the sense that a single logical bit of information is encoded in the entangled state of multiple physical bits~\cite{Nielsen2000}.  These methods are costly in the sense that they lower the density of stored information, slow the effective speed of the computation, and require highly-entangled systems which are difficult to achieve in practice.

%Many types of errors, for instance errors caused by undesired spontaneous emission in trapped atomic ions~\cite{ }, will require QEC.  
However, many types of systematic errors can be corrected using a simple technique that avoids the costly overhead associated with QEC.  Compensating composite pulse sequences reduce systematic errors by choosing operations such that the net error nearly cancels.  These methods require no measurements or additional ancilla bits, and improve accuracy even when the strength of the error is unknown.  Compensating sequences were originally developed in the context of NMR spectroscopy.  Pioneering work in the subject was done by Malcolm Levitt~\cite{Levitt1979, Levitt1981}, Ray Freeman~\cite{Freeman1998}, Robert Tycko~\cite{TyckoPRL1983,Tycko1985},  and others.  These techniques were first introduced to the quantum information community when they were used in NMR quantum computers~\cite{Jones2011}.  Currently several researchers are studying compensating sequences in the context of specific quantum computing models.

%Why have we not yet succeeded in building a practical quantum computer?  The answer is, errors make our lives more difficult.

%\begin{figure}
%\begin{center}
%\includegraphics{figures/chapter1/pdf/figure1}
%\end{center}

%\caption[Timeline of ion-trap quantum computing]{Quantum measurement makes the problem of correcting errors difficult.  During computation   Used with permission. 
%\label{fig:cartoon}}
%\end{figure}

\section{Organization of the thesis}

This thesis develops compensating sequences for ion-trap quantum computers.  We introduce a control-theoretic formalism that successfully generalizes all known fully-compensating composite sequences.  Using this framework, novel sequences which correct systematic errors in ion-trap quantum computers are designed.  We demonstrate these methods in several experiments on trapped atomic ions, and describe how they may be extended to improve pairwise entangling operations.  

The thesis is organized as follows.  Chapter~\ref{sec:chapter2} introduces quantum computing and quantum control theory.  Chapter~\ref{sec:ion-trap-quantum-computing} describes ion-trap quantum computers, surface-electrode traps, and related experimental hardware.  Chapter~\ref{sec:chapter4} introduces compensating pulse sequences, and derives a fundamental set of conditions which all fully-compensating sequences satisfy.  Systematic error models for ion-trap quantum computers are also described here.  Chapter~\ref{sec:chapter5} discusses novel narrowband sequences which correct systematic errors associated with single-ion addressing.  We test these sequences in an experiment on single $^{40}\text{Ca}^+$ ions confined in a surface-electrode trap.  Chapter~\ref{sec:chapter6} describes a surface-electrode trap with on-chip waveguides which produce near-field microwaves to manipulate single $^{171}\text{Yb}^+$ ions.  We demonstrate compensating sequences which correct errors associated with the microwave field.  Chapter~\ref{sec:chapter7} extends the composite pulse technique to include multi-ion interactions.  Finally, chapter~\ref{sec:chapter8} describes a surface electrode trap with an integrated micromirror for efficient fluorescence measurement of trapped ions.

