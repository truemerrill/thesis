Compensating Sequences for Robust Quantum Control of Trapped-Ion Qubits
=======================================================================

Copyright (C) 2013 True Merrill
Georgia Institute of Technology,
School of Chemistry and Biochemistry
Georgia Tech Research Institute
true.merrill@gatech.edu

