Scan over carrier and axial sidebands on 729 nm |S_1/2, -1/2> -> |D_5/2, -5/2> transition.  Doppler cooled, no sideband cooling.  nbar (axial) is probably ~ 12-14.

Laser power: - 15 dB (approximately 500 uW)
pulse duration: 10.4 usec

Frequency axis is in "AOM frequency;" multiply x2 to get true frequency splittings.  Axial splitting should be approximately 1.37 MHz.